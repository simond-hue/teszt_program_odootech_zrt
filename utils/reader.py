import os
import json


class FileReader:
    def __init__(self, directory, file_extension) -> None:
        self._file_extension = file_extension
        self._file_paths = []
        self._json_data = []
        self._collect_paths(directory=directory)


    # Collects all the path path information into a list of strings
    def _collect_paths(self, directory) -> None:
        for path, subdirectories, files in os.walk(directory):
            for name in files:
                if name.endswith(self._file_extension):
                    self._file_paths.append(os.path.join(path, name))


    # Reads all the file contents and returns a list of json objects
    def read_files(self) -> []:
        print('A beolvasás futkorászik...')
        for path in self._file_paths:
            file = open(path)
            read_file = file.read()
            json_object = { **{ 'id': os.path.basename(file.name.strip(self._file_extension)) }, **json.loads(read_file) }
            self._json_data.append(json_object)
        return self._json_data

