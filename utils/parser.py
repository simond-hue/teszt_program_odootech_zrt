from schemas.bycicle import Bycicle
from schemas.car import Car


class Parser:
    TYPE_DATA = {
        'auto': Car,
        'bicikli': Bycicle
    }
    TYPE_NAMES = list(TYPE_DATA.keys())


    def __init__(self, json_data) -> None:
        self._processed_data = []
        self._process(json_data)


    # Parses JSON data into Python object
    def _process(self, json_data) -> None:
        print('Az adatok feldolgozása folyamatban...')
        for data in json_data:
            if(data['type'] in self.TYPE_NAMES):
                self._processed_data.append(self.TYPE_DATA[data['type']](**data))


    # Returns the processed objects
    def get_processed_data(self) -> []:
        return self._processed_data

