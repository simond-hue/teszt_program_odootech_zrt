class Printer:
    @staticmethod
    def print(objects):
        return print(Printer._normal_message(objects)) if len(objects) > 0 else print(Printer._empty_message())
 

    @staticmethod
    def _empty_message():
        return 'Nem található jármű!'
    

    @staticmethod
    def _normal_message(objects):
        str = '\n'.join([object.__str__() for object in objects])
        return 'Járművek adatai:\n' + str
        
