from schemas.vehicle import Vehicle


class Bycicle(Vehicle):
    def __init__(self, id, type, marka, terhelhetoseg) -> None:
        super().__init__(id, type, marka)
        self.terhelhetoseg = terhelhetoseg


    def __str__(self) -> str:
        return super().__str__() + f'Terhelhetőség: {self.terhelhetoseg}'

