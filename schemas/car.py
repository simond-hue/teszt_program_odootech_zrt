from schemas.vehicle import Vehicle


class Car(Vehicle):
    def __init__(self, id, type, marka, ajtok_szama) -> None:
        super().__init__(id, type, marka)
        self.ajtok_szama = ajtok_szama


    def __str__(self) -> str:
        return super().__str__() + f'Ajtók száma: {self.ajtok_szama}'

