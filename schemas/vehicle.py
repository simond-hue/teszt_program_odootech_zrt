class Vehicle:
    def __init__(self, id, type, marka) -> None:
        self.id = id
        self.type = type
        self.marka = marka


    def __str__(self) -> str:
        return f'ID: {self.id}\t Típus: {self.type}\t Márka: {self.marka}\t '

