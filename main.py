from utils.reader import FileReader
from utils.parser import Parser
from utils.printer import Printer


# path to the requested directoru
DIRECTORY_PATH = './data'
FILE_EXTENSION = '.dat'

# reading and parsing file contents
reader = FileReader(DIRECTORY_PATH, FILE_EXTENSION)
parser = Parser(reader.read_files())
Printer.print(parser.get_processed_data())

